﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using teroapi.database;
using teroapi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace teroapi.Controllers
{
    [Route("api/[controller]")]
    public class TeroController : Controller
    {
        // GET: api/values
        [HttpGet]
        public IEnumerable<Teste> GetTestes()
        {
           return new TesteDataAccess().GetTestes();
        }
    }
}
