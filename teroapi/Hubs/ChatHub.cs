﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace teroapi.Hubs
{
    public class ChatHub : Hub
    {
        public override Task OnConnectedAsync()
        {
            Send("You have connected to the chat");
            return base.OnConnectedAsync();
        }

        public void Send(string message)
        {
            // Two argument must use this interfaceSubscriptionHandler2 .
            Clients.All.SendAsync("Send", message);
        }
    }
}
