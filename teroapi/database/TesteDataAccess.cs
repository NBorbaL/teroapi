﻿using System.Collections.Generic;
using System.Data;
using teroapi.Models;

namespace teroapi.database
{
    public class TesteDataAccess : BaseDataAccess { 

        public TesteDataAccess()
        {
        }
        
        public List<Teste> GetTestes()
        {
            List<Teste> testes = new List<Teste>();
            DataTable resultados = ExecuteNonQuery("Select * from TESTE", null);
            if (resultados != null)
            {
                foreach (DataRow resultado in resultados.Rows)
                {
                    testes.Add(new Teste()
                    {
                        id = int.Parse(resultado["ID"].ToString()),
                        nome = resultado["NOME"].ToString()
                    });
                }
            }
            return testes;
        }
    }
}
